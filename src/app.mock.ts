export const unsupportedCommandMsg =
  'Sorry, We only support menu, order <food name> <amount>, and checkout commands.';

export const unsupportedEventType = 'Sorry, this event type is not supported.';
