import { Post, Req, Controller, Param, Get } from '@nestjs/common';
import { Body } from '@nestjs/common';
import { CustomersService } from 'src/customers/customers.service';
import { MenuOrderDTO } from './order.dto';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
  constructor(
    private orderService: OrdersService,
    private customersService: CustomersService,
  ) {}

  @Post()
  async createCustomerOrder(@Body() menuOrder: MenuOrderDTO) {
    const { menuName, menuAmount, userId } = menuOrder;

    return this.orderService.create(menuName, menuAmount, userId);
  }

  @Get(':userId')
  async getUserOrders(@Param('userId') userId: string) {
    return this.orderService.waitingOrders(userId);
  }
  @Post(':userId/checkout')
  async checkoutOrder(@Param('userId') userId: string) {
    return this.orderService.checkout(userId);
  }
}
