import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Menu } from '../menu/menu.entity';
import { Repository } from 'typeorm';
import { Order } from './order.entity';
import { MenuService } from 'src/menu/menu.service';
import { CustomersService } from 'src/customers/customers.service';
import { Customer } from 'src/customers/customer.entity';

interface MenuOrderObj {
  menu: Menu;
  menuAmount: number;
}
interface OrderObject {
  totalPrice: number;
  productOrders: MenuOrderObj[];
}
@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    private readonly menuService: MenuService,
    private readonly customersService: CustomersService,
  ) {}

  async create(
    menuName: string,
    menuAmount: number,
    userId: string,
  ): Promise<string> {
    const menu = await this.menuService.findByName(menuName);

    if (!menu) {
      return "We doesn't have the menu you ordered.";
    }

    const totalPrice = menu.price * menuAmount;

    const customer = await this.customersService.findOne(userId);
    const orderEntity = Order.create();
    orderEntity.customer = customer;
    orderEntity.totalPrice = totalPrice;

    await this.ordersRepository.save(orderEntity);
    return 'Order ' + menuName + ' x' + menuAmount + ' ' + totalPrice + '$';
  }

  async checkout(userId: string): Promise<string> {
    const customer = await this.customersService.findOne(userId);
    const order = await this.sumTotalPrice(customer);
    await this.updateCheckedOutOrders(customer);

    if (!order.sum) {
      return "You don't have any orders yet.";
    }

    return 'Total price = ' + order.sum + '$';
  }

  async waitingOrders(userId: string) {
    const customer = await this.customersService.findOne(userId);
    return this.ordersRepository.find({ where: { customer, checkout: false } });
  }

  async updateCheckedOutOrders(customer: Customer) {
    this.ordersRepository
      .createQueryBuilder('order')
      .update()
      .set({ checkout: true })
      .where({ customer, checkout: false })
      .execute();
  }

  async sumTotalPrice(customer: Customer) {
    return this.ordersRepository
      .createQueryBuilder('order')
      .select('SUM(order.totalPrice)', 'sum')
      .where({ customer, checkout: false })
      .getRawOne();
  }
}
