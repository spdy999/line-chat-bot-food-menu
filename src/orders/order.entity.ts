import { Exclude } from 'class-transformer';
import { Customer } from '../customers/customer.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  totalPrice: number;

  @Column({ default: false })
  checkout: boolean;

  @Exclude()
  @ManyToOne(() => Customer, (customer) => customer.orders, { nullable: false })
  customer: Customer;
}
