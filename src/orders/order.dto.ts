export interface MenuOrderDTO {
  id: number;
  amount: number;
}

export interface CreateUserOrderDTO {
  menus: MenuOrderDTO[];
}

export interface MenuOrderDTO {
  menuName: string;
  menuAmount: number;
  userId: string;
}
