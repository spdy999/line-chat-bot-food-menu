import { Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { AppService } from './app.service';
import { MenuService } from './menu/menu.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly menuService: MenuService,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/webhook')
  getWebhook(@Req() req: Request, @Res() res: Response) {
    const event = req.body.events[0];
    this.appService.events(event);
    return res.sendStatus(200);
  }
}
