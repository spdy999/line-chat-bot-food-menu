import { Controller, Get, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { CustomersService } from './customers.service';

@Controller('customers')
export class CustomersController {
  constructor(private readonly customerService: CustomersService) {}

  @Post('')
  createUser(@Req() req: Request) {
    return this.customerService.insert(req.body.userId);
  }

  @Get('')
  getAll() {
    return this.customerService.findAll();
  }
}
