import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Customer } from './customer.entity';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  async findOne(userId: string): Promise<Customer | undefined> {
    return this.customersRepository.findOne({ userId });
  }

  async findAll(): Promise<Customer[] | undefined> {
    return this.customersRepository.find();
  }

  async insert(userId: string): Promise<Customer> {
    const customerEntity: Customer = Customer.create();
    customerEntity.userId = userId;

    await Customer.save(customerEntity);

    return customerEntity;
  }

  async delete(userId: string): Promise<DeleteResult> {
    return this.customersRepository.delete({ userId });
  }
}
