import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MenuModule } from './menu/menu.module';
import { MenuService } from './menu/menu.service';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, MenuModule],
      controllers: [AppController],
      providers: [AppService, MenuService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getHello()).toBe('Hello World!');
    });
    // it('should return 200', () => {
    //   const req = httpMocks.createRequest({
    //     method: 'GET',
    //     url: ''
    //   })
    //   expect(appController.getWebhook());
    // });
  });
});
