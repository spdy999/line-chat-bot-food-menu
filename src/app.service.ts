import { HttpService, Injectable } from '@nestjs/common';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { EventDTO, PostToLineBody } from './app.dto';
import { unsupportedCommandMsg, unsupportedEventType } from './app.mock';
import { CustomersService } from './customers/customers.service';
import { MenuService } from './menu/menu.service';
import { OrdersService } from './orders/orders.service';

@Injectable()
export class AppService {
  constructor(
    private httpService: HttpService,
    private menuService: MenuService,
    private customerService: CustomersService,
    private orderService: OrdersService,
  ) {}

  getHello(): string {
    return 'Hello World!';
  }

  events(event: EventDTO) {
    const event_type = event.type;

    switch (event_type) {
      case 'follow': {
        // create user
        const userId = event.source.userId;
        this.customerService.insert(userId);
        break;
      }
      case 'unfollow': {
        // delete user
        this.customerService.delete(event.source.userId);
        break;
      }
      case 'message': {
        const replyToken = event.replyToken;
        const msgText = event.message.text;
        const userId = event.source.userId;

        this.reply(replyToken, msgText, userId);
        break;
      }
      default: {
        return unsupportedEventType;
        break;
      }
    }
  }

  async selectResponse(msgText: string, userId: string): Promise<string> {
    const msgArr = msgText.split(' ');
    const commandType = msgArr[0].toLowerCase();
    switch (commandType) {
      case 'menu': {
        return await this.menuService.beautifulMenuMsg();
        break;
      }
      case 'order': {
        // create order
        const menuName = msgArr[1];
        const menuAmount = parseInt(msgArr[2], 10);
        return await this.orderService.create(menuName, menuAmount, userId);
        break;
      }
      case 'checkout': {
        return await this.orderService.checkout(userId);
      }
      default: {
        return unsupportedCommandMsg;
        break;
      }
    }
  }

  async createBody(
    replyToken: string,
    msgText: string,
    userId: string,
  ): Promise<PostToLineBody> {
    const response = await this.selectResponse(msgText, userId);
    return {
      replyToken: replyToken,
      messages: [
        {
          type: 'text',
          text: response,
        },
      ],
    };
  }

  postToLine(body: PostToLineBody) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization:
        'Bearer OwuCqcom5/HvvRrbBMjQB+jVKeTu/Ul5gQ+Dw1Pli/ez6Ur/mfNLAyN7NsSWkI6qxjTufLsu2XpB5yzWH5rCpwQIAK6qVKOyMo93jq+0VD0suP/c1vJjrcB1jCdBDuWColYQCkG2AQKC1eZd61aI3gdB04t89/1O/w1cDnyilFU=',
    };
    const postUrl = 'https://api.line.me/v2/bot/message/reply';

    this.httpService
      .post(postUrl, JSON.stringify(body), { headers })
      .pipe(
        map((resp) => {
          console.log('resp.data = ' + resp.data);
          return resp.data;
        }),
        catchError((err) => {
          console.log('err = ' + err);

          return throwError(err);
        }),
      )
      .toPromise();
  }

  async reply(replyToken: string, msgText: string, userId: string) {
    const body = await this.createBody(replyToken, msgText, userId);

    this.postToLine(body);
  }
}
