import { MenuDTO } from './menu.dto';

export const pizzaMock = {
  name: 'Pizza',
  price: 500,
};
export const menuMock: MenuDTO[] = [
  {
    name: 'Ramen',
    price: 100,
  },
  pizzaMock,
  {
    name: 'Hamburger',
    price: 300,
  },
];
