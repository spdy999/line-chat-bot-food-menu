import { Test, TestingModule } from '@nestjs/testing';
import { menuMock, pizzaMock } from './menu.mock';
import { MenuService } from './menu.service';

describe('MenuService', () => {
  let service: MenuService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MenuService],
    }).compile();

    service = module.get<MenuService>(MenuService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all menu', () => {
    expect(service.findAll()).toBe(menuMock);
  });

  it('should return one menu by its name', () => {
    expect(service.findOne('Pizza')).toBe(pizzaMock);
  });

  it('should return beautiful message for findAll menu', () => {
    const msg = 'Ramen 100 $\nPizza 500 $\nHamburger 300 $';

    expect(service.beautifulMenuMsg()).toBe(msg);
  });
});
