import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MenuDTO } from './menu.dto';
import { Menu } from './menu.entity';
import { menuMock } from './menu.mock';

@Injectable()
export class MenuService {
  constructor(
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
  ) {}

  async findByName(name: string): Promise<Menu> {
    return this.menuRepository.findOne({ name: name });
  }

  async findAll(): Promise<Menu[]> {
    return this.menuRepository.find();
  }

  async findOne(id: number): Promise<Menu> {
    return this.menuRepository.findOne(id);
  }

  async beautifulMenuMsg(): Promise<string> {
    const allMenu = await this.findAll();
    const strArr: string[] = allMenu.map(
      (menu: Menu) => menu.name + ' ' + menu.price + '$',
    );

    return strArr.join('\n');
  }

  async findByIds(ids: number[]): Promise<Menu[]> {
    return this.menuRepository.findByIds(ids);
  }
}
