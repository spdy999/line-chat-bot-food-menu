export interface MenuDTO {
  name: string;
  price: number;
}
