export interface MessageDTO {
  type: 'text';
  text: string;
}
export interface PostToLineBody {
  replyToken: string;
  messages: MessageDTO[];
}

export interface SourceDTO {
  userId: string;
  type: 'user';
}

export interface EventDTO {
  type: 'follow' | 'unfollow' | 'message';
  replyToken?: string;
  source: SourceDTO;
  timestamp: number;
  mode: 'active';
  message?: MessageDTO;
}
