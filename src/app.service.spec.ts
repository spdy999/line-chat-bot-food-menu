import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { unsupportedCommandMsg } from './app.mock';
import { AppService } from './app.service';
import { menuMock } from './menu/menu.mock';
import { MenuModule } from './menu/menu.module';
import { MenuService } from './menu/menu.service';

describe('AppService', () => {
  let appService: AppService;
  let menuService: MenuService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, MenuModule],
      controllers: [AppController],
      providers: [AppService, MenuService],
    }).compile();

    appService = app.get<AppService>(AppService);
    menuService = app.get<MenuService>(MenuService);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appService.getHello()).toBe('Hello World!');
    });
  });

  describe('selectResponse', () => {
    it('should return menu response', () => {
      expect(appService.selectResponse('menu')).toBe(
        menuService.beautifulMenuMsg(),
      );
    });

    it('should return unimplemented message', () => {
      expect(appService.selectResponse(':)')).toBe(unsupportedCommandMsg);
    });
  });

  describe('createBody', () => {
    it('should return correct menu for line bot body', () => {
      expect(appService.createBody('replyToken', 'menu')).toStrictEqual({
        replyToken: 'replyToken',
        messages: [
          {
            type: 'text',
            text: menuService.beautifulMenuMsg(),
          },
        ],
      });
    });

    it('should return unsupported command message for line bot body', () => {
      expect(appService.createBody('replyToken', ':)')).toStrictEqual({
        replyToken: 'replyToken',
        messages: [
          {
            type: 'text',
            text: unsupportedCommandMsg,
          },
        ],
      });
    });
  });
});
