import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MenuModule } from './menu/menu.module';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/customer.entity';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/order.entity';
import { Menu } from './menu/menu.entity';

@Module({
  imports: [
    HttpModule,
    MenuModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.POSTGRES_HOST || '127.0.0.1',
      port: 5432,
      username: process.env.POSTGRES_USERNAME || 'avenger999',
      password: process.env.POSTGRES_PASSWORD || '',
      database: process.env.POSTGRES_DATABASE || 'linebot',
      entities: [Customer, Menu, Order],
      synchronize: true,
    }),
    CustomersModule,
    OrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
