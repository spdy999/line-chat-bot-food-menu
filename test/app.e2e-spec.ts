import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { AppService } from './../src/app.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let appsService = {
    getHello: (): string => 'Hello World!',
    reply: (reply_token: string, msg: string): void => {},
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(AppService)
      .useValue(appsService)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  it('/webhook (POST)', () => {
    return request(app.getHttpServer())
      .post('/webhook')
      .send({
        events: [{ replyToken: 'xxxxreplyTokenxxxx', message: 'echo message' }],
      })
      .expect(200);
  });
});
