# Line bot

## Play with bot

add line account @589taeqw

- command `menu` to get all menu
- command `order <food name> <amount>` to order
  - example : `order Pizza 2`
- command `checkout` to get total price

# Set up project

- install dependencies
  - `yarn`
- start project
  - `yarn start`

---
This project is deployed on heroku and use postgres as a database. The server might response slowly when it starting to wake up.